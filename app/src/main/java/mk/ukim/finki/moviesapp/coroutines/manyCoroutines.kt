package mk.ukim.finki.moviesapp.coroutines

import kotlinx.coroutines.*
import kotlin.concurrent.thread
import kotlin.system.measureTimeMillis

fun main() = runBlocking {
    val time = measureTimeMillis {
        testTime()
    }
    println()
    println(time)
}

suspend fun testTime() = coroutineScope {
    repeat(100000) {
        launch(Dispatchers.IO){
            delay(100)
            println(Thread.currentThread().name)
        }
    }
}

fun testTimeThread() {
    repeat(100000) {
        thread {
            Thread.sleep(100)
//            print(".")
        }
    }
}