package mk.ukim.finki.moviesapp.coroutines


import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() = runBlocking {
    doWorld()
}

suspend fun doWorld() = coroutineScope {
    val job1 = launch {
        delay(1000)
        println("World 1!")
    }

    val job2 = launch {
        delay(2000)
        println("World 2!")
    }

    println("Hello")
    job1.join()
    job2.join()
    println("Done!")
}


