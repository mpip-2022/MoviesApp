package mk.ukim.finki.moviesapp.domain.movie.room

import androidx.room.*
import mk.ukim.finki.moviesapp.domain.movie.model.Movie

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(movie: Movie)

    @Delete
    suspend fun deleteMovie(movie:Movie)

    @Query("DELETE FROM movie where id=:id")
    suspend fun delete(id: Int)

    @Query("SELECT * FROM movie")
    suspend fun getAll(): List<Movie>

    @Query("SELECT * FROM movie WHERE title LIKE :query")
    suspend fun searchMovies(query: String): List<Movie>

}